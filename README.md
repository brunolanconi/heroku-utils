# heroku-utils

This folder groups utilities to work with Heroku.

## Dependencies
- [Docker](https://docs.docker.com/get-docker/)
- *Optionally* [Make](https://www.gnu.org/software/make/)

## Base

The base utilities are:

```bash
auth-token                     Initiates heroku-utils container and runs heroku auth:token
bash                           Initiates heroku-utils container and runs bash
build                          Builds heroku-utils container as heroku-utils
env-var                        Initiates heroku-utils container and sets environment variable on Heroku
help                           Show help
logs                           Initiates heroku-utils container and runs heroku logs
make-backup                    Initiates heroku-utils container and makes backup
schedule-backup                Initiates heroku-utils container and schedules backup
```

## Poetry

The poetry utilities are:

```bash
poetry-bash                    Initiates heroku-utils-poetry container and runs bash
poetry-create-superuser        Initiates heroku-utils-poetry container and creates Django super user
poetry-download-db-as-sqlite3  Initiates heroku-utils-poetry container and downloads Django db as sqlite3
```

## Getting started

- Clone this repository within your Heroku project;
- Update the `.env` file with your Heroku app name. You may use the `.env.example` as a template;
- Update the `.netrc` file with your Heroku credentials. You may use the `.netrc.example` as a template;
- On project root, Create the `heroku.env` file with the desired environment variables that will be set on Heroku app. You may use the `heroku.env.example` as a template;
- *Optionally* copy the desired commands from `Makefile` to your project's root `Makefile`.

### Poetry project

`poetry-*` Makefile commands relies on `poetry.Dockerfile` which is a Dockerfile to build a container with Poetry installed. This image is based on `heroku-utils` image.

#### Requirements

- The root project must have a `pyproject.toml` file;
- The root project must have a `README.md` file;


```Dockerfile
# utils/poetry/poetry.Dockerfile

...
COPY ./pyproject.toml .
COPY ./README.md .
...
```

- `poetry-*` commands relies on the following dependencies:

```toml
# ../pyproject.toml

...
[tool.poetry.dependencies]
django = "^5.0"
...
[tool.poetry.group.dev.dependencies]
db-to-sqlite = "^1.5"
...
```

- `poetry-create-superuser` command relies on `DJANGO_SUPERUSER_USERNAME`, `DJANGO_SUPERUSER_EMAIL` and `DJANGO_SUPERUSER_PASSWORD` environment variables to create a super user on Django project. So it must exists within your Heroku app, which may be set by `env-var` command and updating the project root `heroku.env` file;
- `poetry-create-superuser` command relies on `manage.py` existing on the root project.

```shell
# utils/poetry/entrypoint.sh

...
create-superuser)
    exec heroku run -a ${APP_NAME} "python manage.py createsuperuser --username ${DJANGO_SUPERUSER_USERNAME} --noinput"
    ;;  
...
```

- `poetry-download-db-as-sqlite3` command relies on `DATABASE_URL` environment variable to download the database as sqlite3. So it must exists within your Heroku app, which is usually set by [Bucketeer plug-in](https://devcenter.heroku.com/articles/bucketeer#app-config).
- `poetry-download-db-as-sqlite3` command relies on `manage.py` existing on the root project.

```shell
# utils/poetry/entrypoint.sh

...
download-db-as-sqlite3)
    cp db.sqlite3 $(date +"%Y-%m-%d_%H-%M-%S").sqlite3.bak || true
    rm db.sqlite3 || true
    poetry install --with dev
    poetry run python manage.py migrate
    poetry run db-to-sqlite $(heroku config --app ${APP_NAME} | grep DATABASE_URL | cut -d: -f 2-) db.sqlite3 --all -p
    chmod 777 db.sqlite3
    ;; 
...
```
