FROM ubuntu:22.04

ENV DEBIAN_FRONTEND noninteractive

# Install Heroku CLI
RUN apt-get update && apt-get install -y curl gnupg git
RUN curl https://cli-assets.heroku.com/install-ubuntu.sh | sh
RUN curl -sSL https://get.docker.com/ | sh

# Copy entrypoint
COPY ./heroku-utils/entrypoint.sh .
RUN chmod a+x ./entrypoint.sh

# Set safe directory
RUN git config --global --add safe.directory /heroku-utils

# Set entrypoint
ENTRYPOINT [ "./entrypoint.sh" ]