#!/bin/bash

set -e

function print_help {
    echo "Available options:"
    echo "bash - Runs bash"
    echo "create-superuser - Creates a superuser on the Django app"
    echo "download-db-as-sqlite3 - Downloads the Heroku database and saves it as a SQLite3 file"
    exit 1
}

cd /heroku-utils

case ${1} in


    # General commands

    bash)
        exec bash
        ;;
       
    
    # Django commands

    create-superuser)
        exec heroku run -a ${APP_NAME} "python manage.py createsuperuser --username ${DJANGO_SUPERUSER_USERNAME} --noinput"
        ;;    
    download-db-as-sqlite3)
        cp db.sqlite3 $(date +"%Y-%m-%d_%H-%M-%S").sqlite3.bak || true
        rm db.sqlite3 || true
        poetry install --with dev
        poetry run python manage.py migrate
        poetry run db-to-sqlite $(heroku config --app ${APP_NAME} | grep DATABASE_URL | cut -d: -f 2-) db.sqlite3 --all -p
        chmod 777 db.sqlite3
        ;;


    # Help
    
    *)
        print_help
    ;;
esac