#!/bin/bash

set -e

function print_help {
    echo "Available options:"
    echo "auth-token - Prints the Heroku auth token"
    echo "bash - Runs bash"
    echo "env-var - Sets the environment variables from the .env file on Heroku"
    echo "logs - Shows the logs of the Heroku app"
    echo "make-backup - Makes a backup of the Heroku app"
    echo "schedule-backup - Schedules a daily backup of the Heroku app"
    exit 1
}

cd /heroku-utils

case ${1} in


    # General commands

    auth-token)
        exec heroku auth:token
        ;;
    bash)
        exec bash
        ;;
    env-var)
        while IFS= read -r line; do
            key=$(echo $line | cut -d= -f1)
            value=$(echo $line | cut -d= -f2)
            echo "Setting ${key}=${value} on Heroku"
            heroku config:set --app=${APP_NAME} ${key}="${value}"
        done < heroku.env
        ;;
    logs)
        exec heroku logs -a ${APP_NAME} --tail
        ;;
    make-backup)
        exec heroku pg:backups:capture --app ${APP_NAME}
        ;;
    schedule-backup)
        exec heroku pg:backups:schedule DATABASE_URL --at '02:00 America/New_York' --app ${APP_NAME}
        ;;
    

    # Help
    
    *)
        print_help
    ;;
esac