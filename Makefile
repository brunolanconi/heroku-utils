help: ## Show help
	# From https://marmelab.com/blog/2016/02/29/auto-documented-makefile.html
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n",$$1,$$2}'

auth-token: ## Initiates heroku-utils container and runs heroku auth:token
	docker compose -f heroku-utils/docker-compose.yml run --build --rm heroku-utils auth-token

bash: ## Initiates heroku-utils container and runs bash
	docker compose -f heroku-utils/docker-compose.yml run --build --rm heroku-utils bash

build: ## Builds heroku-utils container as heroku-utils
	docker compose -f heroku-utils/docker-compose.yml build heroku-utils

env-var: ## Initiates heroku-utils container and sets environment variable on Heroku
	docker compose -f heroku-utils/docker-compose.yml run --build --rm heroku-utils env-var

logs: ## Initiates heroku-utils container and runs heroku logs
	docker compose -f heroku-utils/docker-compose.yml run --build --rm heroku-utils logs

make-backup: ## Initiates heroku-utils container and makes backup
	docker compose -f heroku-utils/docker-compose.yml run --build --rm heroku-utils make-backup

schedule-backup: ## Initiates heroku-utils container and schedules backup
	docker compose -f heroku-utils/docker-compose.yml run --build --rm heroku-utils schedule-backup


# Poetry

poetry-bash: build ## Initiates heroku-utils-poetry container and runs bash
	docker compose -f heroku-utils/docker-compose.yml run --build --rm heroku-utils-poetry bash

poetry-create-superuser: build ## Initiates heroku-utils-poetry container and creates Django super user
	docker compose -f heroku-utils/docker-compose.yml run --build --rm heroku-utils-poetry create-superuser

poetry-download-db-as-sqlite3: build ## Initiates heroku-utils-poetry container and downloads Django db as sqlite3
	docker compose -f heroku-utils/docker-compose.yml run --build --rm heroku-utils-poetry download-db-as-sqlite3