FROM heroku-utils

ENV DEBIAN_FRONTEND noninteractive

# Install python 3.10
RUN apt-get update && apt-get install -y python3.10 python3-pip python3.10-venv

# Intall poetry
RUN pip install poetry

# Copy source code
COPY . .
COPY ./pyproject.toml .
COPY ./README.md .

# Poetry install
RUN poetry install --with dev

# Create requirements
RUN poetry self add poetry-plugin-export
RUN poetry export --without-hashes --format=requirements.txt > requirements.txt

# Install dependencies
RUN pip install --upgrade pip
RUN pip install -r requirements.txt

# Copy entrypoint
COPY ./heroku-utils/utils/poetry/entrypoint.sh .
RUN chmod a+x ./entrypoint.sh